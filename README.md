[français](#markdown-header-seminaire-paraview) | [english](#markdown-header-paraview-seminar)
# Séminaire ParaView

Ce sont les fichier demo qui démontrent l'utilisation de ParaView.

Les fichiers qui se terminent avec « pvsm » sont des fichiers d'état de ParaView. Vous pourriez les ouvrir avec ParaView sous « File » et « Load State... ». Mais pour ouvrir un fichier PVSM, vous avez aussi besoin de l'ensemble de données. Vous pourriez le télécharger à [sciviscontest2018.org](https://sciviscontest2018.org).

Le fichier « datawrite.py » montre que vous pouvez écrire un fichier XDMF qui décrit les données binaires.

Les fichiers « submit.sh » et « animate.py » sont utilisés pour soumettre des tâches de rendu aux grappes de Calcul Canada. Pour les utiliser, connectez-vous à une grappe de Calcul Canada et tapez

    sbatch submit.sh

Cette commande soumettra la tâche à la queue et, une fois démarré, il chargera paraview et démarre le script « animate.py » ce qui peut rendre l'animation.

Dans la dossier « dambreak », vous trouvez les fichiers configurations pour la simulation OpenFOAM d'une rupture d'un digue.

# ParaView seminar

These are demo files demonstrating the use of ParaView

The files the end with "pvsm" are ParaView state files. You can open those with ParaView under "File" and "Load State...". But to open a PVSM file, you also need the dataset. You can download it from [sciviscontest2018.org](https://sciviscontest2018.org).

The file datawrite.py shows how to create an XDMF file describing the binary data.

The files submit.sh and animate.py are used to submit rendering jobs to Compute Canada clusters. To use, log into a Compute Canada cluster and type

    sbatch submit.sh

This will submit the job to the queue and, once running, it will load paraview and start the animate.py script which in turn renders the animation.

In the folder "dambreak", the configuration files for the OpenFOAM simulation for a dam break can be found.
