# import the simple module from the paraview
from paraview.simple import *
# disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

#resolution = [1280, 720]
resolution = [7680, 4320]

# load state
LoadState('/scratch/jarno/Data/impact.pvsm', DataDirectory='/scratch/jarno/Data')

# get animation scene
animationScene1 = GetAnimationScene()

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
renderView1.ViewSize = resolution

# Go to the first frame of the animation amd export as image
animationScene1.GoToFirst()
SaveScreenshot('/scratch/jarno/frames/frame.0000.png', renderView1, ImageResolution=resolution)

# Loop over the other frames and export
for i in range(1, 476):
  animationScene1.GoToNext()
  SaveScreenshot('/scratch/jarno/frames/frame.%04d.png' % i, renderView1, ImageResolution=resolution)
