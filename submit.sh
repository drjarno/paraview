#!/bin/bash

#SBATCH --time=4:30:00
#SBATCH --ntasks=1
#SBATCH --mem=8G
#SBATCH --account=def-jarno

module load paraview-offscreen/5.5.2
cd /scratch/jarno/Data
pvbatch --force-offscreen-rendering --mesa-swr-avx2 animate.py
