#!/usr/bin/env python3

import numpy as np
import struct

dx = 0.1
dy = 0.2
x = 10
y = 30

# Create some data
temperaturedata = np.array([[xp**2 + yp for xp in np.arange(0, x+dx, dx)] for yp in np.arange(0, y+dy, dy)])
pressuredata = np.array([[np.sin(xp) + np.sin(yp) for xp in np.arange(0, x+dx, dx)] for yp in np.arange(0, y+dy, dy)])

# Save the data in binary format
temperaturedata.astype('float64').tofile('temperature.bin')
pressuredata.astype('float64').tofile('pressure.bin')

# Write the accompying XDMF describing the binary data
# See http://www.xdmf.org/index.php/XDMF_Model_and_Format for the format
with open("data.xdmf", "w") as f:
  f.write('<?xml version="1.0" ?>\n')
  f.write('<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>\n')
  f.write('<Xdmf Version="3.0">\n')
  f.write('  <Domain>\n')
  f.write('    <Grid Name="Grid" GridType="Uniform">\n')
  f.write('      <Topology TopologyType="2DCoRectMesh" Dimensions="%d %d" />\n' % (temperaturedata.shape[0], temperaturedata.shape[1]))
  f.write('      <Geometry GeometryType="ORIGIN_DXDY">\n')
  f.write('        <DataItem Name="Origin" Format="XML" Dimensions="3">0 0</DataItem>\n')
  f.write('        <DataItem Name="Spacing" Format="XML" Dimensions="3">%f %f</DataItem>\n' % (dy, dx))
  f.write('      </Geometry>\n')
  f.write('      <Attribute Name="T" AttributeType="Scalar" Center="Node">\n')
  f.write('        <DataItem Format="Binary" Dimensions="%ld" NumberType="Float" Precision="8">\n' % temperaturedata.size)
  f.write('          temperature.bin\n')
  f.write('        </DataItem>\n')
  f.write('      </Attribute>\n')
  f.write('      <Attribute Name="P" AttributeType="Scalar" Center="Node">\n')
  f.write('        <DataItem Format="Binary" Dimensions="%ld" NumberType="Float" Precision="8">\n' % pressuredata.size)
  f.write('          pressure.bin\n')
  f.write('        </DataItem>\n')
  f.write('      </Attribute>\n')
  f.write('    </Grid>\n')
  f.write('  </Domain>\n')
  f.write('</Xdmf>\n')
